import { BootMixin } from "@loopback/boot";
import { ApplicationConfig } from "@loopback/core";
import { RestApplication } from "@loopback/rest";
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from "@loopback/rest-explorer";
import path from "path";
import { RecipeServiceBindings } from "./keys";
import { MySequence } from "./sequence";
import { RecipeService } from "./services/recipes.services";

export { ApplicationConfig };

export class RecipesGeneratorApplication extends BootMixin(RestApplication) {
  constructor(options: ApplicationConfig = {}) {
    super(options);
    this.setUpBindings();
    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static("/", path.join(__dirname, "../public"));

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: "/explorer",
    });
    this.component(RestExplorerComponent);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ["controllers"],
        extensions: [".controller.js"],
        nested: true,
      },
    };
  }
  setUpBindings(): void {
    this.bind(RecipeServiceBindings.RECIPE_SERVICE).toClass(RecipeService);
  }
}
