import { inject } from "@loopback/core";
import {
  get,
  post,
  Request,
  response,
  ResponseObject,
  RestBindings,
} from "@loopback/rest";
import { RecipeServiceBindings } from "../keys";
import { RecipeData, RecipeService } from "../services/recipes.services";

/**
 * OpenAPI response for ping()
 */
const PING_RESPONSE: ResponseObject = {
  description: "Ping Response",
  content: {
    "application/json": {
      schema: {
        type: "object",
        title: "PingResponse",
        properties: {
          greeting: { type: "string" },
          date: { type: "string" },
          url: { type: "string" },
          headers: {
            type: "object",
            properties: {
              "Content-Type": { type: "string" },
            },
            additionalProperties: true,
          },
        },
      },
    },
  },
};

/**
 * A simple controller to bounce back http requests
 */
export class RecipesController {
  constructor(
    @inject(RestBindings.Http.REQUEST)
    private req: Request,
    @inject(RecipeServiceBindings.RECIPE_SERVICE)
    private recipeService: RecipeService
  ) {}

  // Map to `GET /ping`
  @get("/ping")
  @response(200, PING_RESPONSE)
  ping(): object {
    // Reply with a greeting, the current time, the url, and request headers
    return {
      greeting: "Hello from LoopBack",
      date: new Date(),
      url: this.req.url,
      headers: Object.assign({}, this.req.headers),
    };
  }

  @post("create-recipe")
  async createRecipe(): Promise<any> {
    let data: RecipeData = {
      documentData: {
        hash: "Soy un recipe",
        code128: "123456",
        city: "Bogota",
        clinicLogo: "logo.png",
        emissionDate: "23/04/2021 - 11:00 Hrs",
      },
      professionalData: {
        address: "Las Rosas 654, Pudahuel, Metropolitina",
        email: "matiaszilleruelo@gmail.com",
        identification: "15.576.456-3",
        name: "MATIAS IGNACIO ZILLERUELO",
        specialty: "TRAUMATOLOGÍA",
        phone: "+ 56 9 5657487",
        sign: "sign.png",
      },
      patientData: {
        name: "Rodrigo Cerda Fernandez",
        address: "Av. El Chercán 456, Santiago, Metropolitina0",
        identification: "16.465.476-2",
        age: "24",
        birthdate: "24 de Octubre de 1988",
        phone: "+56 9 5673527",
        sex: "MASCULINO",
      },
      recipeData: [
        {
          description: "Tomar cada 2 horas",
          name: "Ibuprofeno",
        },
        {
          description: "Tomar cada 2 horas",
          name: "Ampicilina",
        },
      ],
    };
    await this.recipeService.generateRecipe(data);
    return {
      code: 200,
      message: "Ok",
    };
  }
}
