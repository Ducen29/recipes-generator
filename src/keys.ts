import { BindingKey } from "@loopback/core";
import { RecipeService } from "./services/recipes.services";

export namespace RecipeServiceBindings {
  export const RECIPE_SERVICE = BindingKey.create<RecipeService>(
    "services.recipes.service"
  );
}
