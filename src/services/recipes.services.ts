import { BindingScope, injectable } from "@loopback/context";
import { createCanvas } from "canvas";
import fs from "fs";
import pdf, { CreateOptions } from "html-pdf";
import JsBarcode from "jsbarcode";
import path from "path";
import QRCode from "qrcode";

export interface RecipeData {
  documentData: {
    emissionDate: string;
    city: string;
    hash: string;
    code128: string;
    clinicLogo: string;
  };
  patientData: {
    name: string;
    birthdate: string;
    age: string;
    identification: string;
    sex: string;
    address: string;
    phone: string;
  };
  professionalData: {
    name: string;
    specialty: string;
    identification: string;
    address: string;
    email: string;
    phone: string;
    sign: string;
  };
  recipeData: RecipeDataList[];
}

export interface RecipeDataList {
  name: string;
  description: string;
}

@injectable({ scope: BindingScope.TRANSIENT })
export class RecipeService {
  async generateRecipe(data: RecipeData) {
    let html = fs.readFileSync(
      path.resolve(process.cwd(), "templates/recipe.html"),
      "utf8"
    );
    html = await this.header(html, data?.documentData);
    html = await this.footer(html, data?.documentData, data?.professionalData);
    html = this.setPatientData(html, data?.patientData);
    html = this.setlist(html, data?.recipeData);
    console.log(html);
    let options: CreateOptions = {
      height: "1062px",
      width: "612px",
      footer: {
        height: "110mm",
      },
    };
    pdf.create(html, options).toFile("./recipe.pdf", (err, res) => {
      if (err) return console.log(err);
      console.log(res);
    });
  }

  async header(html: string, documentData?: any) {
    let qr = await QRCode.toDataURL(documentData.hash);
    let code = await this.getBarCode(documentData.code128);
    let buffer = fs.readFileSync(
      path.resolve(process.cwd(), "templates/" + documentData.clinicLogo)
    );
    let buffer2 = fs.readFileSync(
      path.resolve(process.cwd(), "templates/AvenirLTStd-Heavy.otf")
    );
    let buffer3 = fs.readFileSync(
      path.resolve(process.cwd(), "templates/background.png")
    );
    let buffer4 = fs.readFileSync(
      path.resolve(process.cwd(), "templates/AvenirLTStd-Black.otf")
    );
    let buffer5 = fs.readFileSync(
      path.resolve(process.cwd(), "templates/AvenirLTStd-Book.otf")
    );
    let buffer6 = fs.readFileSync(
      path.resolve(process.cwd(), "templates/AvenirLTStd-Roman.otf")
    );
    let logo =
      "data:image/png;base64," + Buffer.from(buffer).toString("base64");
    let font =
      "data:font/ttf;base64," + Buffer.from(buffer2).toString("base64");
    let fontBlack =
      "data:font/ttf;base64," + Buffer.from(buffer4).toString("base64");
    let fontBook =
      "data:font/ttf;base64," + Buffer.from(buffer5).toString("base64");
    let fontRoman =
      "data:font/ttf;base64," + Buffer.from(buffer6).toString("base64");
    let bg = "data:image/png;base64," + Buffer.from(buffer3).toString("base64");
    html = html.replace("{{logo}}", logo);
    html = html.replace("{{bg}}", bg);
    html = html.replace("{{font}}", font);
    html = html.replace("{{fontBlack}}", fontBlack);
    html = html.replace("{{fontBook}}", fontBook);
    html = html.replace("{{fontRoman}}", fontRoman);
    html = html.replace("{{barCode}}", code);
    html = html.replace("{{qrCode}}", qr);
    html = html.replace("{{date}}", documentData.emissionDate);
    html = html.replace("{{city}}", documentData.city);
    return html;
  }
  async footer(html: string, documentData: any, professionalData: any) {
    let buffer = fs.readFileSync(
      path.resolve(process.cwd(), "templates/" + professionalData.sign)
    );
    let sign = "data:image/png;base64," + new Buffer(buffer).toString("base64");
    html = html.replace("{{sing}}", sign);
    let buffer2 = fs.readFileSync(
      path.resolve(process.cwd(), "templates/" + documentData.clinicLogo)
    );
    let logo =
      "data:image/png;base64," + new Buffer(buffer2).toString("base64");
    html = html.replace("{{logo}}", logo);
    html = html.replace("{{professionalName}}", professionalData.name);
    html = html.replace("{{specialty}}", professionalData.specialty);
    html = html.replace("{{identification}}", professionalData.identification);
    html = html.replace("{{address}}", professionalData.address);
    html = html.replace("{{phone}}", professionalData.phone);
    html = html.replace("{{email}}", professionalData.email);
    html = html.replace("{{code}}", documentData.code128);
    return html;
  }

  async getBarCode(hash: string) {
    let canvas = createCanvas(200, 50);
    JsBarcode(canvas, hash, { height: 50, text: "" });
    let code = canvas.toDataURL();
    return code;
  }

  setPatientData(html: string, patientData: any) {
    html = html.replace("{{patientName}}", patientData.name);
    html = html.replace("{{sex}}", patientData.sex);
    html = html.replace("{{birthdate}}", patientData.birthdate);
    html = html.replace(
      "{{patientIdentification}}",
      patientData.identification
    );
    html = html.replace("{{age}}", patientData.age);
    html = html.replace("{{patientAddress}}", patientData.address);
    html = html.replace("{{patientPhone}}", patientData.phone);
    return html;
  }

  setlist(html: string, recipeData: RecipeDataList[]) {
    let list = "";
    let listStrings = recipeData.map((recipe) => {
      return `
          <table>
          <thead>
            <tr>
              <th style="text-align:left; padding-left: 30px; padding-top: 12px; padding-bottom:11px;">Medicamento: ${recipe.name}</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <p class="font-roman" style="padding-left: 30px">
                  Indicación: ${recipe.description}
                </p>
              </td>
            </tr>
          </tbody>
        </table>
        <br />
      `;
    });
    list = listStrings.join("");
    html = html.replace("{{list}}", list);
    return html;
  }
}
